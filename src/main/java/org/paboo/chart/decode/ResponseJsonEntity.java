/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.paboo.chart.decode;

import java.io.Serializable;

public class ResponseJsonEntity implements Serializable {
    private static final long serialVersionUID = 6749461375995738193L;

    private String rawtext;
    private byte[] rawbytes;
    private String format;
    private String parsedresult;

    public String getRawtext() {
        return rawtext;
    }

    public void setRawtext(String rawtext) {
        this.rawtext = rawtext;
    }

    public byte[] getRawbytes() {
        return rawbytes;
    }

    public void setRawbytes(byte[] rawbytes) {
        this.rawbytes = rawbytes;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getParsedresult() {
        return parsedresult;
    }

    public void setParsedresult(String parsedresult) {
        this.parsedresult = parsedresult;
    }
}
