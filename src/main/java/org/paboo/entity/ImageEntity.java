/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.paboo.entity;

import java.io.Serializable;

/**
 * Image size entity
 *
 * @author Leonard
 */
public class ImageEntity implements Serializable {
    private int height;
    private int width;
    private int margin;
    private int fgColor;
    private int bgColor;

    public ImageEntity() {
    }

    public ImageEntity(int height, int width, int margin, int fgColor, int bgColor) {
        this.height = height;
        this.width = width;
        this.margin = margin;
        this.fgColor = fgColor;
        this.bgColor = bgColor;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getMargin() {
        return margin;
    }

    public void setMargin(int margin) {
        this.margin = margin;
    }

    public int getFgColor() {
        return fgColor;
    }

    public void setFgColor(int fgColor) {
        this.fgColor = fgColor;
    }

    public int getBgColor() {
        return bgColor;
    }

    public void setBgColor(int bgColor) {
        this.bgColor = bgColor;
    }

    @Override
    public String toString() {
        return "ImageEntity{" +
                "height=" + height +
                ", width=" + width +
                ", margin=" + margin +
                ", fgColor=" + fgColor +
                ", bgColor=" + bgColor +
                '}';
    }
}
